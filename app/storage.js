import {ApplicationSettings} from '@nativescript/core';

export function addTask(name , tasks){
    const array = JSON.stringify(tasks);
    
    ApplicationSettings.setString(name, array);
}
export function showTask(name){
    const tasks = ApplicationSettings.getString(name);

    if(tasks) {
        return JSON.parse(tasks);
    }
    else return [];
}

