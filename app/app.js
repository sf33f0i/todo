import Vue from 'nativescript-vue'

import Home from './lists/lists.vue'

new Vue({
  render: (h) => h('frame', [h(Home)]),
}).$start()
